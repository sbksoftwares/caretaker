# Generated by Django 2.0 on 2018-02-11 21:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('supplier', '0001_initial'),
        ('tax', '0001_initial'),
        ('discount', '0001_initial'),
        ('customer', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PurchaseInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=256)),
                ('status', models.CharField(blank=True, choices=[('007', 'Paid'), ('419', 'Unpaid'), ('600', 'Returned'), ('202', 'Overdue'), ('212', 'Partly Paid'), ('000', 'Voided')], default='419', max_length=32)),
                ('terms', models.TextField(blank=True)),
                ('due_date', models.DateField(blank=True, null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(auto_now=True, null=True)),
                ('discount', models.ManyToManyField(blank=True, to='discount.Discount')),
                ('employee', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('supplier', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='supplier.Supplier')),
                ('tax', models.ManyToManyField(blank=True, to='tax.Tax')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PurchaseReturnInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=256)),
                ('status', models.CharField(blank=True, choices=[('007', 'Paid'), ('419', 'Unpaid'), ('600', 'Returned'), ('202', 'Overdue'), ('212', 'Partly Paid'), ('000', 'Voided')], default='419', max_length=32)),
                ('terms', models.TextField(blank=True)),
                ('due_date', models.DateField(blank=True, null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(auto_now=True, null=True)),
                ('discount', models.ManyToManyField(blank=True, to='discount.Discount')),
                ('employee', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('purchasesinvoice', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='invoice.PurchaseInvoice')),
                ('supplier', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='supplier.Supplier')),
                ('tax', models.ManyToManyField(blank=True, to='tax.Tax')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SalesInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=256)),
                ('status', models.CharField(blank=True, choices=[('007', 'Paid'), ('419', 'Unpaid'), ('600', 'Returned'), ('202', 'Overdue'), ('212', 'Partly Paid'), ('000', 'Voided')], default='419', max_length=32)),
                ('terms', models.TextField(blank=True)),
                ('due_date', models.DateField(blank=True, null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(auto_now=True, null=True)),
                ('customer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='customer.Customer')),
                ('discount', models.ManyToManyField(blank=True, to='discount.Discount')),
                ('employee', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('tax', models.ManyToManyField(blank=True, to='tax.Tax')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SalesReturnInvoice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=256)),
                ('status', models.CharField(blank=True, choices=[('007', 'Paid'), ('419', 'Unpaid'), ('600', 'Returned'), ('202', 'Overdue'), ('212', 'Partly Paid'), ('000', 'Voided')], default='419', max_length=32)),
                ('terms', models.TextField(blank=True)),
                ('due_date', models.DateField(blank=True, null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_modified', models.DateTimeField(auto_now=True, null=True)),
                ('customer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='customer.Customer')),
                ('discount', models.ManyToManyField(blank=True, to='discount.Discount')),
                ('employee', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
                ('salesinvoice', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='invoice.SalesInvoice')),
                ('tax', models.ManyToManyField(blank=True, to='tax.Tax')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
