# Generated by Django 2.0.6 on 2018-07-30 00:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('osm', '0001_initial'),
        ('pos', '0004_auto_20180725_0146'),
    ]

    operations = [
        migrations.AddField(
            model_name='cashsale',
            name='branch',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.BussinessBranch'),
        ),
        migrations.AddField(
            model_name='cashsale',
            name='bussiness',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.Bussiness'),
        ),
        migrations.AddField(
            model_name='cashsalesreturn',
            name='branch',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.BussinessBranch'),
        ),
        migrations.AddField(
            model_name='cashsalesreturn',
            name='bussiness',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.Bussiness'),
        ),
        migrations.AddField(
            model_name='creditsale',
            name='branch',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.BussinessBranch'),
        ),
        migrations.AddField(
            model_name='creditsale',
            name='bussiness',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.Bussiness'),
        ),
        migrations.AddField(
            model_name='creditsalesreturn',
            name='branch',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.BussinessBranch'),
        ),
        migrations.AddField(
            model_name='creditsalesreturn',
            name='bussiness',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='osm.Bussiness'),
        ),
    ]
